package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"
)

type cmdParams struct {
	name    string
	args    []string
	env     []string
	timeout time.Duration
	stdOut  *bytes.Buffer
	stdErr  *bytes.Buffer
}

type gitLog struct {
	Commit               string `json:"commit"`
	AbbreviatedCommit    string `json:"abbreviated_commit"`
	Tree                 string `json:"tree"`
	Parent               string `json:"parent"`
	AbbreviateParent     string `json:"abbreviated_parent"`
	Refs                 string `json:"refs"`
	Encoding             string `json:"encoding"`
	Subject              string `json:"subject"`
	SanitizedSubjectLine string `json:"sanitized_subject_line"`
	Body                 string `json:"body"`
	CommitNotes          string `json:"commit_notes"`
	VerificationFlag     string `json:"verification_flag"`
	Signer               string `json:"signer"`
	SignerKey            string `json:"signer_key"`
	Author               struct {
		Name  string `json:"name"`
		Email string `json:"email"`
		Date  string `json:"date"`
	} `json:"author"`
	Commiter struct {
		Name  string `json:"name"`
		Email string `json:"email"`
		Date  string `json:"date"`
	} `json:"commiter"`
}

func TestMain(t *testing.T) {
	cmdList := make([]string, 3)
	cmdList[0] = "git"
	cmdList[1] = "log"
	cmdList[2] = `--pretty=format:{%n  "commit": "%H",%n  "abbreviated_commit": "%h",%n  "tree": "%T",%n  "abbreviated_tree": "%t",%n  "parent": "%P",%n  "abbreviated_parent": "%p",%n  "refs": "%D",%n  "encoding": "%e",%n  "subject": "%s",%n  "sanitized_subject_line": "%f",%n  "body": "%b",%n  "commit_notes": "%N",%n  "verification_flag": "%G?",%n  "signer": "%GS",%n  "signer_key": "%GK",%n  "author": {%n    "name": "%aN",%n    "email": "%aE",%n    "date": "%aD"%n  },%n  "commiter": {%n    "name": "%cN",%n    "email": "%cE",%n    "date": "%cD"%n  }%n}`
	cmd := &cmdParams{
		name: cmdList[0],
		args: cmdList[1:],
		env: []string{
			fmt.Sprintf("%s=%s", "PWD", os.Getenv("PWD")),
		},
		timeout: 30 * time.Second,
		stdOut:  new(bytes.Buffer),
		stdErr:  new(bytes.Buffer),
	}
	if err := cmd.execute(); err != nil {
		t.Fatal(err)
	}
	if stdErr := cmd.stdErr.String(); stdErr != "" {
		t.Fatal(stdErr)
	}
	stdOut := cmd.stdOut.String()
	stdOut = strings.Trim(stdOut, "\n")

	data := new(gitLog)
	if err := json.Unmarshal(cmd.stdOut.Bytes(), &data); err != nil {
		t.Fatal(err)
	}
	expectedBranch := "HEAD -> master"
	if data.Refs != expectedBranch {
		t.Fatalf("For %v \n expected %s\n got %s", data, expectedBranch, data.Refs)
	}
	fmt.Printf("Result: %+v\n", data)
}

func (param *cmdParams) execute() error {

	ctx, cancel := context.WithTimeout(context.Background(), param.timeout)
	defer cancel()

	cmd := exec.CommandContext(ctx, param.name, param.args...)

	cmd.Stdout = param.stdOut
	cmd.Stderr = param.stdErr
	cmd.Env = append(os.Environ(), param.env...)
	return cmd.Run()
}
